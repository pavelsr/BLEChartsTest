package com.example.testapp.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.jjoe64.graphview.GraphView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class DemoChartsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_charts);
    }

    InputStream is = null;
    try {
        is = getAssets().open("samples_100hz.txt");
    } catch (IOException e) {
        e.printStackTrace();
    }
    Scanner sc = new Scanner(is);           // java.util.Scanner is standart class for input data
    List<GraphView.GraphViewData> lines = new ArrayList<GraphView.GraphViewData>();     // what does <> means?
    double t=0;
    while (sc.hasNextLine()) {
        String number = sc.nextLine();
        if (TextUtils.isDigitsOnly(number) && !TextUtils.isEmpty(number)) {
            lines.add(new GraphViewData(t,Double.parseDouble(number)));
            t += 0.01;      // step
        }
    }

    GraphViewSeries.GraphViewSeriesStyle style = new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(200, 50, 0), 2);
    ADCseries = new GraphViewSeries("ADC raw", style, lines.toArray(new GraphViewData[]{}));
    LinearLayout layout = (LinearLayout) findViewById(R.id.time_graph);     // what does (LinearLayout) means?
    layout.addView(graphView);


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.demo_charts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
